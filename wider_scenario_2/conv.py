import pandas as pd
import matplotlib.pyplot as plt
import lorawan_sat_functions as lw

df_converted = pd.read_csv('my_satellite_LLA.csv')


# Graficar el recorrido
plt.plot(df_converted['Longitude'], df_converted['Latitude'], marker='o', linestyle='-', color='b')
plt.xlim([-110, -35])
plt.xlabel("LON (deg)")
plt.ylim([-60, 15])
plt.ylabel("LAT (deg)")
plt.grid()
plt.draw()
plt.show()

# Ruta del archivo CSV con las coordenadas XYZ


# Llama a la función para generar el recorrido y guardar el archivo CSV convertido



#def generate_satellite_path(input_file):
    # Cargar el archivo CSV
#    df = pd.read_csv(input_file)

    # Crear un DataFrame para almacenar los datos convertidos
#    df_converted = pd.DataFrame()

    # Procesar cada fila del DataFrame original
#    for index, row in df.iterrows():
#        lat, lon, alt = lw.ecef_to_lla(row['X[km]'], row['Y[km]'], row['Z[km]'])
#        df_converted.loc[index, 'Latitude'] = lat
#        df_converted.loc[index, 'Longitude'] = lon
#        df_converted.loc[index, 'Altitude'] = alt

    # Guardar el DataFrame convertido en un nuevo archivo CSV
#    df_converted.to_csv('my_satellite_LLA.csv', index=False)

    # Graficar el recorrido
#    plt.plot(df_converted['Longitude'], df_converted['Latitude'], marker='o', linestyle='-', color='b')
#    plt.xlim([-110, -35])
#    plt.xlabel("LON (deg)")
#    plt.ylim([-60, 15])
#    plt.ylabel("LAT (deg)")
#    plt.grid()
 #   plt.draw()
  #  plt.show()

# Ruta del archivo CSV con las coordenadas XYZ
#input_file = 'my_sateliter_LEO-POS.csv'

# Llama a la función para generar el recorrido y guardar el archivo CSV convertido
#generate_satellite_path(input_file)
