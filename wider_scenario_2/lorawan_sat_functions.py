#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 12:41:27 2022

@author: guido
"""
#import geopy
import math

###functions
def ecef_to_lla (x,y,z):
    x = x*1000 #in meters
    y = y*1000 #in meters
    z = z*1000 #in meters
    
    a = 6378137.0 #in meters
    b = 6356752.314245 #in meters
    
    f = (a - b) / a
    f_inv = 1.0 / f
    
    e_sq = f * (2 - f)                       
    eps = e_sq / (1.0 - e_sq)
    
    p = math.sqrt(x * x + y * y)
    q = math.atan2((z * a), (p * b))
    
    sin_q = math.sin(q)
    cos_q = math.cos(q)
    
    sin_q_3 = sin_q * sin_q * sin_q
    cos_q_3 = cos_q * cos_q * cos_q
    
    phi = math.atan2((z + eps * b * sin_q_3), (p - e_sq * a * cos_q_3))
    lam = math.atan2(y, x)
    
    v = a / math.sqrt(1.0 - e_sq * math.sin(phi) * math.sin(phi))
    h   = (p / math.cos(phi)) - v
    
    lat = math.degrees(phi)
    lon = math.degrees(lam)
    
    #print(lat,lon,h)
    return (lat,lon,h)



def lla_to_ecef(lat, lon, alt):
    rad_lat = lat * (math.pi / 180.0)
    rad_lon = lon * (math.pi / 180.0)

    a = 6378137.0
    finv = 298.257223563
    f = 1 / finv
    e2 = 1 - (1 - f) * (1 - f)
    v = a / math.sqrt(1 - e2 * math.sin(rad_lat) * math.sin(rad_lat))

    x = (v + alt) * math.cos(rad_lat) * math.cos(rad_lon) /1000
    y = (v + alt) * math.cos(rad_lat) * math.sin(rad_lon) /1000
    z = (v * (1 - e2) + alt) * math.sin(rad_lat) /1000

    return x, y, z #en km

# this function computes the airtime of a packet
# according to LoraDesignGuide_STD.pdf

#Check out https://loratools.nl/#/airtime

def airtime(sf,cr,pl,bw):
    H = 0        # implicit header disabled (H=0) or not (H=1)
    DE = 0       # low data rate optimization enabled (=1) or not (=0)
    Npream = 8   # number of preamble symbol (12.25  from Utz paper)

    if bw == 125 and sf in [11, 12]:
        # low data rate optimization mandated for BW125 with SF11 and SF12
        DE = 1
    if sf == 6:
        # can only have implicit header with SF6
        H = 1

    Tsym = (2.0**sf)/bw  # msec
    Tpream = (Npream + 4.25)*Tsym
    #print (">> Has sf {}, cr {}, pl {}, bw {}".format(sf,cr,pl,bw))
    payloadSymbNB = 8 + max(math.ceil((8.0*pl-4.0*sf+28+16-20*H)/(4.0*(sf-2*DE)))*(cr+4),0)
    Tpayload = payloadSymbNB * Tsym
    return ((Tpream + Tpayload)/1000.0)  # to secs
    #return 0.368896 #hardcoded
#
# this function creates a node
#
