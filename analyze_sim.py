#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thr 25/01/2024

@author: german
"""
import numpy as np
import matplotlib.pyplot as plt
import csv
import os

lista = []
datos = [[]]

save = 1

archivo_entrada = "pruba_pre_1CH_p3_500_rndm_sf\RS_1_500_16_p3.csv"
# Dividir el nombre del archivo de entrada en carpeta y csv
carpeta, parte_csv = archivo_entrada.split("\\")

with open(archivo_entrada, "r") as reader:
    lista = reader.read().splitlines()

for line in lista:
    datos.append(line.split(","))

del datos[0]

sent = 0
beacons = 0
lost = 0
collided = 0
no_processed = 0
extracted = 0
percentage_extracted = 0
not_reach_beacon = 0

for line in lista:
    if "B" in line:
        beacons += 1
    elif "PL" or "PC" or "NP" or "PE" or "R" in line:
        sent += 1
        if "PL" in line:
            lost += 1
        if "PC" in line:
            collided += 1
        if "NP" in line:
            no_processed += 1
        if "PE" in line:
            extracted += 1
        if "R" in line:
            not_reach_beacon += 1

percentage_extracted = (extracted / sent) * 100

print("Total beacons: ", beacons)
print("Total Sent: ", sent)
print("Total lost: ", lost)
print("Total collided: ", collided)
print("Total No Processed: ", no_processed)
print("Total Extracted/Received: ", extracted)
print('Total nodes with not reach beacon', not_reach_beacon)
print(f"Percentage of Packets Extracted Successfully: {percentage_extracted:.2f}")

nombre_archivo_salida_csv = "prueba-pre.csv"

# Guardar resultados en un archivo CSV
def archivo_existe(nombre_archivo):
    return os.path.isfile(nombre_archivo)

if archivo_existe(nombre_archivo_salida_csv) & save == 1:
    with open(nombre_archivo_salida_csv, mode='a', newline='') as archivo_csv:
        writer = csv.writer(archivo_csv)
        # Escribir una nueva fila con los datos
        writer.writerow([beacons, sent, lost, collided, no_processed, extracted, not_reach_beacon, f"{percentage_extracted:.2f}%", carpeta, parte_csv])
        print(f"Datos agregados al archivo {nombre_archivo_salida_csv}.")
else:
    with open(nombre_archivo_salida_csv, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['beacons', 'sent', 'lost', 'collided', 'no_processed', 'extracted', 'cant_reach_beacon', 'percentage_extracted', 'carpeta', 'archivo csv'])
        # Escribir resultados
        writer.writerow([beacons, sent, lost, collided, no_processed, extracted, not_reach_beacon, f"{percentage_extracted:.2f}%", carpeta, parte_csv])

    print(f"Resultados guardados en {nombre_archivo_salida_csv}")

